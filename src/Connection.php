<?php

namespace Vicimus\FTP;

use Vicimus\FTP\Exceptions\ConnectionException;

/**
 * Handles many FTP functions
 *
 * @author Jordan
 */
class Connection
{
    /**
     * Holds the connection handle
     *
     * @var Connection
     */
    protected $conn;

    /**
     * Indicates if the user has been authenticated
     *
     * @var bool
     */
    protected $authenticated = false;

    /**
     * Store the credentials
     *
     * @var string[]
     */
    protected $credentials = [
        'user' => null,
        'pass' => null,
    ];

    /**
     * Create a new connection with a url and optionally a user and password
     *
     * @param string $url  The URL to connect to
     * @param string $user The username
     * @param string $pass The password
     */
    public function __construct($url = null, $user = null, $pass = null)
    {
        if ($url) {
            $this->connect($url);
        }

        $this->credentials = [
            'user' => $user,
            'pass' => $pass,
        ];

        if ($this->isConnected() && $user) {
            $this->login();
        }
    }

    /**
     * Close the connection if still open
     */
    public function __destruct()
    {
        if ($this->isConnected()) {
            ftp_close($this->conn);
        }
    }

    /**
     * Check if the Connection instance is connected to a server
     *
     * @return bool
     */
    public function isConnected()
    {
        return !is_null($this->conn);
    }

    /**
     * Check if the User authenticated successfully
     *
     * @return bool
     */
    public function isAuthenticated()
    {
        return $this->authenticated;
    }

    /**
     * Upload a file to the current connection
     *
     * @param string $local  Path to the file to update
     * @param string $remote The path to save the remote file
     * @param int    $mode   The file transfer mode
     *
     * @return bool
     */
    public function upload($local, $remote = null, $mode = FTP_ASCII)
    {
        $this->checkConnection();

        if (!file_exists($local)) {
            throw new \InvalidArgumentException(
                'Local file '.$local.' does not exist'
            );
        }

        if (!$remote) {
            $remote = $this->filename($local);
        }

        return ftp_put($this->conn, $remote, $local, $mode);
    }

    /**
     * Extract the filename from the local file to use as the remote file
     *
     * @param string $local The local filename
     *
     * @return string
     */
    protected function filename($local)
    {
        if (stripos($local, '/') === false) {
            return $local;
        }

        $remote = substr($local, strrpos($local, '/'));
        return $remote;
    }

    /**
     * List the contents of a directory
     *
     * @param string $directory The directory to list
     *
     * @return array
     */
    public function listing($directory = '.')
    {
        $this->checkConnection();
        return ftp_nlist($this->conn, $directory);
    }

    /**
     * Connect to a server
     *
     * @param string $url The URL to connect to
     *
     * @return bool
     */
    public function connect($url)
    {
        $this->conn = ftp_connect($url);
        if ($this->isConnected()) {
            ftp_set_option($this->conn, FTP_TIMEOUT_SEC, 5);
        }

        return $this->isConnected();
    }

    /**
     * Login to the currently connected FTP server
     *
     * @param string $username The username to use
     * @param string $password The password to use
     *
     * @return bool
     */
    public function login($username = null, $password = null)
    {
        $this->checkConnection();

        if (is_null($username)) {
            $username = $this->credentials['user'];
            $password = $this->credentials['pass'];
        }

        $this->authenticated = false;
        if (@ftp_login($this->conn, $username, $password)) {
            $this->authenticated = true;
            ftp_pasv($this->conn, true);
        }

        return $this->authenticated;
    }

    /**
     * Checks to see if the connection is active
     *
     * @return void
     */
    protected function checkConnection()
    {
        if (!$this->isConnected()) {
            throw new ConnectionException('No connection established');
        }
    }
}

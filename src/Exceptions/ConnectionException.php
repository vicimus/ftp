<?php

namespace Vicimus\FTP\Exceptions;

/**
 * Thrown when an action is taken that requires a connection but a connection
 * is not present
 */
class ConnectionException extends \Exception
{
    //
}

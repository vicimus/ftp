<?php

namespace Vicimus\FTP\Tests;

use Vicimus\FTP\Connection;
use Vicimus\FTP\Exceptions\ConnectionException;

/**
 * Test the FTP class
 *
 * @author Jordan
 */
class FTPTest extends TestCase
{
    /**
     * Test constructing a new FTP class
     *
     * @return void
     */
    public function testInstantiate()
    {
        $ftp = new Connection;
        $this->assertInstanceOf(Connection::class, $ftp);
    }

    /**
     * Test connecting to an FTP server
     *
     * @return void
     */
    public function testConnect()
    {
        $ftp = new Connection(
            $this->env('FTP_URL'),
            $this->env('FTP_USER'),
            $this->env('FTP_PASS')
        );

        $this->assertTrue($ftp->isConnected());
        $this->assertTrue($ftp->isAuthenticated());
    }

    /**
     * Test uploading a file
     *
     * @return void
     */
    public function testUpload()
    {
        $file = __DIR__.'/ConnectionTest.php';

        $ftp = new Connection(
            $this->env('FTP_URL'),
            $this->env('FTP_USER'),
            $this->env('FTP_PASS')
        );

        $result = $ftp->upload($file);
        $this->assertTrue($result);
    }

    /**
     * Test listing the contents of a directory
     *
     * @return void
     */
    public function testList()
    {
        $ftp = new Connection(
            $this->env('FTP_URL'),
            $this->env('FTP_USER'),
            $this->env('FTP_PASS')
        );

        $result = $ftp->listing();
        $this->assertInternalType('array', $result);
    }

    /**
     * Should throw an exception
     *
     * @return void
     */
    public function testCannotConnect()
    {
        $file = __DIR__.'/ConnectionTest.php';

        try {
            $ftp = new Connection(
                null,
                null,
                null
            );

            $ftp->upload($file);
            $this->fail('No exception thrown');
        } catch (ConnectionException $ex) {
            //
        }
    }
}

<?php

namespace Vicimus\FTP\Tests;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;

/**
 * Base test class that offers a few utility methods.
 *
 * @author Jordan Grieve <jgrieve@vicimus.com>
 */
abstract class TestCase extends PHPUnitTestCase
{
    /**
     * Load the .env file
     */
    public function __construct()
    {
        $dotenv = new \Dotenv\Dotenv(__DIR__.'/../');
        $dotenv->load();
    }

    /**
     * Get an env value
     *
     * @param string $name    The name of the value to get
     * @param mixed  $default The default value to return if not found
     *
     * @return mixed
     */
    public function env($name, $default = null)
    {
        return getenv($name) ?: $default;
    }
}
